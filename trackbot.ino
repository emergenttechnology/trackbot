// Tracked Robot
// Chris Moran (chris DOT moran AT emergent DOT com DOT au
//
// This project is designed to suit the common Tamiya Track Chassis available from
// a variety of resellers.  I am assuming a two-port L298N motor drive unit is connected.
//
// This branch of the code also supports an HMC5883 Compass Module.  There are no code
// requirements, simply connect the device as per the schematic, and the software will
// detect and enable it.

#define ARDUINO_NANO

#include <Servo.h>
#include <NewPing.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>

#define DEBUG

#define HMC5883         0x1E

#ifdef ARDUINO_UNO
// Right Motor
#define MOTOR_A_ENABLE  6
#define MOTOR_A_IN_1    7
#define MOTOR_A_IN_2    8

// Left Motor
#define MOTOR_B_ENABLE  11
#define MOTOR_B_IN_1    12
#define MOTOR_B_IN_2    13

// Ultrasonics
#define TRIG_PIN 5
#define ECHO_PIN 9

// Servo
#define SERVO_PIN       5
#endif

#ifdef ARDUINO_NANO
// Right Motor
#define MOTOR_A_ENABLE  3
#define MOTOR_A_IN_1    2
#define MOTOR_A_IN_2    4

// Left Motor
#define MOTOR_B_ENABLE  5
#define MOTOR_B_IN_1    7
#define MOTOR_B_IN_2    8

// Ultrasonics
#define TRIG_PIN        9
#define ECHO_PIN        10

// Servo
#define SERVO_PIN       11
#endif

#define MAX_DISTANCE 200
#define MAX_SPEED 100
#define COLL_DIST 30
#define TURN_DIST COLL_DIST + 20

// Initialise the sonar module
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);

// Define our sensor
Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(12345);

// Create a servo controller
Servo servo;

unsigned int leftRange;
unsigned int rightRange;
unsigned int frontRange;
bool hasNav = false;

void setup() {
#ifdef DEBUG
  Serial.begin(115200);
#endif
  pinMode(MOTOR_A_ENABLE, OUTPUT);
  pinMode(MOTOR_A_IN_1, OUTPUT);
  pinMode(MOTOR_A_IN_2, OUTPUT);
  pinMode(MOTOR_B_ENABLE, OUTPUT);
  pinMode(MOTOR_B_IN_1, OUTPUT);
  pinMode(MOTOR_B_IN_2, OUTPUT);
#ifdef DEBUG
  Serial.println("Ports set");
#endif

  // Reset the servo and aim the sonar head
#ifdef DEBUG
  Serial.println("Servo Start");
#endif
  servo.attach(SERVO_PIN);
  servo.write(0);
  delay(1000);
  servo.write(180);
  delay(1000);
  servo.write(90);
  delay(1000);

  // Set up the compass module
#ifdef DEBUG
  Serial.println("Compass Start");
#endif
  if(!mag.begin()) {
#ifdef DEBUG
      Serial.println("No compass found!  Nav disabled.");
      hasNav = false;
#endif
  }
  else {
#ifdef DEBUG
    Serial.println("Compass Started");
#endif
    hasNav = true;  
  }
  delay(300);
#ifdef DEBUG
  Serial.println("Ready for main");
#endif
}

void loop() {
  servo.write(90);
  delay(90);
  frontRange = readRange();
  if(frontRange <= COLL_DIST) {
#ifdef DEBUG
    Serial.println("Obstacle detected");
#endif
    steer();
  }
  else {
#ifdef DEBUG
    Serial.println("Moving");
#endif
    forward(MAX_SPEED);
  }
}

// Measure distance to any obstacle
int readRange() {
  unsigned int uS = 0;
  unsigned int dist = 0;
  uS = sonar.ping();
  dist = uS/US_ROUNDTRIP_CM;
  return dist;
}

void steer() {
  // Hit the brakes first!
  stop();

  // Now, take a "look around"
  servo.write(36);
  delay(500);     // For damping
  leftRange = readRange();
  servo.write(144);
  delay(500);
  rightRange = readRange();
  // Return to centre
  servo.write(90);

  // Now, work out where to go
  if(leftRange < rightRange) {
    if(hasNav == true) {
      newHeading(-45.0);
    }
    else {
      right(MAX_SPEED / 2);
      delay(250);
      stop();
    }
  }
  else if(rightRange < leftRange) {
    if(hasNav == true) {
      newHeading(45.0);
    }
    else {
      left(MAX_SPEED / 2);
      delay(250);
      stop();
    }
  }
  else {  // The same, so turn around (more or less)
    if(hasNav == true) {
      newHeading(180.0);
    }
    else {
      reverse(MAX_SPEED / 2);
      delay(250);
      stop();
      right(MAX_SPEED / 2);
      delay(500);
      stop();
    }
  }
}

//
// Abstraction for motors
//

void stop() {
#ifdef DEBUG
  Serial.println("stop()");
#endif
  motorASpeed(0);
  motorBSpeed(0);
  motorAStop();
  motorBStop();  
}

void forward(int speed) {
#ifdef DEBUG
  Serial.println("forward()");
#endif
  motorAForward();
  motorBForward();
  motorASpeed(speed);
  motorBSpeed(speed);
}

void reverse(int speed) {
#ifdef DEBUG
  Serial.println("reverse()");
#endif
  motorAReverse();
  motorBReverse();
  motorASpeed(speed);
  motorBSpeed(speed);
}

// TODO:
// For the turns, we now accept an 
// angle, which is the compass heading away from
// our present value.
// It will be easiest to integrate the ADAFruit HMC5883 library
// because it can give us a heading value.

void newHeading(float offset) {
  float currentHeading = getHeading();

  // Adjust our values in case the adjusted
  // course would wrap the 360/0 point
  if((currentHeading - offset) < 0)
    currentHeading += 360;

  if((currentHeading + offset) > 360)
    currentHeading -= 360;

  if(offset == 0)
    return;
  
  if(offset < 0) {
    while(getHeading() > (currentHeading - offset))
      left(MAX_SPEED / 4);
  }
  if(offset > 0) {
    while(getHeading() < (currentHeading + offset))
      right(MAX_SPEED / 4);
  }
}

void left(int speed) {
#ifdef DEBUG
  Serial.println("left()");
#endif
  motorAForward();
  motorBReverse();
  motorASpeed(speed);
  motorBSpeed(speed);
}

void right(int speed) {
#ifdef DEBUG
  Serial.println("right()");
#endif
  motorAReverse();
  motorBForward();
  motorASpeed(speed);
  motorBSpeed(speed);
}

void motorAForward() {
  digitalWrite(MOTOR_A_IN_1, HIGH);
  digitalWrite(MOTOR_A_IN_2, LOW);
}

void motorAReverse() {
  digitalWrite(MOTOR_A_IN_1, LOW);
  digitalWrite(MOTOR_A_IN_2, HIGH);
}

void motorAStop() {
  digitalWrite(MOTOR_A_IN_1, LOW);
  digitalWrite(MOTOR_A_IN_2, LOW);
}

void motorASpeed(int speed) {
  analogWrite(MOTOR_A_ENABLE, speed);
}

void motorBForward() {
  digitalWrite(MOTOR_B_IN_1, HIGH);
  digitalWrite(MOTOR_B_IN_2, LOW);
}

void motorBReverse() {
  digitalWrite(MOTOR_B_IN_1, LOW);
  digitalWrite(MOTOR_B_IN_2, HIGH);
}

void motorBStop() {
  digitalWrite(MOTOR_B_IN_1, LOW);
  digitalWrite(MOTOR_B_IN_2, LOW);
}

void motorBSpeed(int speed) {
  analogWrite(MOTOR_B_ENABLE, speed);
}

float getHeading() {
  sensors_event_t event;
  mag.getEvent(&event);
  float heading = 0.00;
  float headingDegrees = 0.00;
#ifdef DEBUG
  /* Display the results (magnetic vector values are in micro-Tesla (uT)) */
  Serial.print("X: "); Serial.print(event.magnetic.x); Serial.print("  ");
  Serial.print("Y: "); Serial.print(event.magnetic.y); Serial.print("  ");
  Serial.print("Z: "); Serial.print(event.magnetic.z); Serial.print("  ");Serial.println("uT");
#endif

  heading = atan2(event.magnetic.y, event.magnetic.x);
  // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
  // Find yours here: http://www.magnetic-declination.com/
  // Mine is: -13* 2' W, which is ~13 Degrees, or (which we need) 0.22 radians
  // If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
  float declinationAngle =  -0.22;
  heading += declinationAngle;
  // Correct for when signs are reversed.
  if(heading < 0)
  heading += 2*PI;
  
// Check for wrap due to addition of declination.
if(heading > 2*PI)
  heading -= 2*PI;

  headingDegrees = heading * 180/M_PI;
#ifdef DEBUG
  Serial.print("Heading: ");
  Serial.println(headingDegrees);
#endif
  return headingDegrees;
}
