# README #

## Introduction ##
This is a simple autonomous robot using skid steering.  It is written for use with a Tamiya track chassis and dual-motor gearbox, but it is equally suitable to other two-wheel platform.

Collision avoidance is via a servo-steered ultrasonic module.  You will need the Arduino "NewPing" library.

### Motor Control ###
This project assumes a basic L298N bridge driver.  These are available from many sources, and all work in the same basic way.

Please note, you *can* drive a micro-servo direct from the Arduino, but I am not convinced it's a good idea.  Better to seek out a motor driver board which include the servo driver (just a power transistor really) to protect your CPU

